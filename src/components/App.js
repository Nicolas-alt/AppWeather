import React from "react";
import CurrentWeather from "../components/CurrentWeather";
import PlaceWeather from "../components/PlaceWeather";

export const App = _ => {
  let error = true;
  return (
    <>
      {error
        ? <PlaceWeather />
        : <CurrentWeather />
      }
    </>
  );
}

export default App;
