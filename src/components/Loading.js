import React from 'react'

export const Loading = _ => {
  return (
    <div class="d-flex justify-content-center text-primary">
      <div class="spinner-border" role="status">
        <span class="sr-only">Obteniendo ubicación...</span>
      </div>
    </div>
  );
}

export default Loading;
