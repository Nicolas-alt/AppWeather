import React, {useState} from 'react'

export const PlaceWeather = () => {
  const [data, setData] = useState({
    city: '',
    country: ''
  });

  const {city, country} = data;

  const getInputData = (e) => {
    e.preventDefault()
    console.log(data)
  }

  const handleInputChange = ({target}) => {
    setData({
      ...data,
      [target.name]: target.value
    })
  }

  return (
    <form onSubmit={getInputData}>
      <h2>Completa los campos para obtener el clima</h2>
      <input onChange={handleInputChange} placeholder="City" name="city" value={city} />
      <input onChange={handleInputChange} placeholder="Country" name="country" value={country} />
    </form>
  )
}

export default PlaceWeather;
