import {key} from '../keys/keys'

const {latitude, longitude} = location;
const getCurrentWeather = async (latitude, longitude) => {
  try {
    const url = `https://api.openweathermap.org/data/2.5/weather?lat=${latitude}&lon=${longitude}&units=metric&appid=${key}`
    const resWeather = await fetch(url);
    const data = await resWeather.json();
    const {main: {temp, temp_min, temp_max, humidity}, name} = data;
  } catch (error) {
    console.log(error);
  }
}

