import {key} from '../keys/keys'

export const useFetchPlace = ({city}) => {
  const url = `api.openweathermap.org/data/2.5/weather?q=${encodeURI(city)}&appid=${key}`
  fetch(url)
    .then(response => response.json())
    .then(data => {data})
}

