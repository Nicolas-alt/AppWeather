export const getLocalitation = () => {

  const success = (pos) => {
    let crd = pos.coords
    let datos = {
      latitude: crd.latitude,
      longitude: crd.longitude,
    }
    return datos
  }

  const error = (err) => {
    console.warn('ERROR(' + err.code + '): ' + err.message);
    let errApp = true;
    return errApp;
  };

  return navigator.geolocation.getCurrentPosition(success, error);
}

