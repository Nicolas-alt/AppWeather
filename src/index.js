import React from 'react';
import ReactDOM from 'react-dom';
import './assets/sass/index.scss';

ReactDOM.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById('root')
);

